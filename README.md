## Overview

A simple script to determine the classification accuracy from multiple feature files using a summation score from multiple classifiers

Try running with all of the sample feature files:

`$ python fusion.py features/click.csv features/motion.csv features/scroll.csv features/keystroke.csv`

Mix and match files to get different accuracies:

`$ python fusion.py features/click.csv features/scroll.csv`

The performance generally goes down with the number of modalities, although this isn't always the case.

TODO: Try different classifier fusion algorithms: instead of summation, used the mean or median. See how this affects the accuracy.