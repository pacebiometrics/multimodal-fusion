'''
Created on Sep 25, 2014

@author: vinnie
'''

import sys
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve
from scipy import interp
import matplotlib.pyplot as plt
from IPython import embed

def multi_knn(p, k=5, out=None):
    '''
    Leave-one-out classification results for multimodal features
    '''
    results = {} # will become a DataFrame later
    
    # Class labels in lexicographic order, used below
    labels = np.sort(p.major_axis.levels[0].values)
    
    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    
    # Create a classifier for each modality (data frame) in the panel
    knns = {i:KNeighborsClassifier(n_neighbors=k) for i in p.items}
    for idx in p.major_axis:
        # Create a new space with the sample left out
        p_loo = p.drop(idx, axis=1)
        
        # Train each classifier - no work required for a KNN
        [knns[name].fit(df.values, df.index.get_level_values(0)) for name,df in p_loo.iteritems()]
        
        # Get a prediction from each classifier
        all_results = np.array([knns[name].predict_proba(p[name].loc[idx].values) for name,df in p_loo.iteritems()])
        
        # Summation rule used here
        class_proba = all_results.sum(axis=0)[0]
        # Take the class with the highest probability (most neighbors)
        results[idx] = labels[class_proba.argmax()]
        
        labels_bin = label_binarize([idx[0]], labels).ravel()
        fpr, tpr, thresh = roc_curve(labels_bin, class_proba)
        
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0
        
    # Cleanup the results
    results = pd.DataFrame.from_dict(results, orient='index')
    results.index = pd.MultiIndex.from_tuples(results.index)
    results.index.names = p.major_axis.names
    results.columns = ['result']
    
    if out:
        results.to_csv(out)
    
    print('Accuracy score:', accuracy_score(results.index.get_level_values(0), results['result']))
    
    return results

def load_panel(fnames, indices=['user','session']):
    '''
    Load a bunch of CSV files into a dataframe
    Important: the first column should be the class label! 
    Everything else that isn't an index column is used as a feature
    '''
    dfs = {}
    for fname in fnames:
        dfs[fname] = pd.read_csv(fname, index_col=indices)
    
    # Missing or mismatched features are set to 0, which does not affect the KNN results.
    # This may not be the case for a different classifier though.
    p = pd.Panel.from_dict(dfs).fillna(0)
    
    return p

if __name__ == '__main__':
    fnames = sys.argv[1:]
    p = load_panel(fnames)
    results = multi_knn(p)